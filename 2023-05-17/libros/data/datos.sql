﻿DROP DATABASE IF EXISTS libros;
CREATE DATABASE libros;
USE libros;

CREATE TABLE libros(
idLibro int AUTO_INCREMENT,
nombre varchar(100),
idAutor int,
paginas int,
PRIMARY KEY(idLibro)
);

create TABLE autores(
idAutor int AUTO_INCREMENT,
nombre varchar(100),
poblacion varchar(100),
PRIMARY KEY(idAutor)

);

INSERT INTO libros 
  (nombre, idAutor, paginas) VALUES 
  ('Libro 1', 1, 200);

INSERT INTO autores 
(nombre, poblacion) VALUES 
('Autor 1', 'Santander'),
('Autor 22','Laredo');
