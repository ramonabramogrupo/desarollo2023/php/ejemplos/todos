<?php


/**
 * funcion que permite conectarse a una base de datos
 *
 * @param  string $baseDatos nombre de la base de datos
 * @return object Conexion a la base de datos
 */
function conectar($baseDatos)
{
    // creamos una conexion
    // creamos un objeto de tipo conexion
    return new mysqli(
        "localhost", // servidor de base de datos
        "root", // usuario
        "", // contraseña
        $baseDatos, // base de datos
        3306 // puerto
    );
}


/**
 * Me permite ejecutar cualquier consulta de sql
 *
 * @param  object $conexion objeto que apunta a la conexion
 * @param  string $consulta consulta a ejecutar
 * @return object objeto de tipo mysqli_result
 */
function consulta($conexion, $consulta)
{
    return $conexion->query($consulta);
}

/**
 * Me permite ejecutar cualquier consulta de sql
 *
 * @param  object $conexion objeto que apunta a la conexion
 * @param  string $consulta consulta a ejecutar
 * @return array me devuelve un array con los registros
 */
function consultaArray($conexion, $consulta)
{
    return $conexion->query($consulta)->fetch_all(MYSQLI_ASSOC);
}
