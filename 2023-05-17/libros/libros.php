<?php
require_once "librerias/conexion.inc";
require_once "librerias/utilidades.inc";

$conexion = conectar("libros");

$resultados = consultaArray(
    $conexion,
    "select * from libros"
);

$titulo = "Libros";
$h1 = "Listado de libros";
$contenido = gridView($resultados);

require_once "plantilla.php";
