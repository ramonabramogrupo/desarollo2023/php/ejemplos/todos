<?php
require_once "librerias/conexion.inc";
require_once "librerias/utilidades.inc";

$conexion = conectar("libros");
$resultados = consultaArray(
    $conexion,
    "select * from autores"
);

$titulo = "autores";
$h1 = "Listado de autores";
$contenido = gridView($resultados);

require_once "plantilla.php";
