<?php
require_once "librerias/utilidades.inc";
require_once "librerias/conexion.inc";
$conexion = conectar("ventas");

$titulo = "Clientes";
$h1 = "Gestion de clientes";
$registros = consultaArray(
    $conexion,
    "select * from cliente"
);
$contenido = gridView($registros);

require_once "plantilla.php";
