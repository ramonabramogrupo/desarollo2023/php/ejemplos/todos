<?php
require_once "librerias/utilidades.inc";
require_once "librerias/conexion.inc";

$conexion = conectar("ventas");

$titulo = "Pedidos";
$h1 = "Gestion de pedidos";
$registros = consultaArray(
    $conexion,
    "select * from pedido"
);
$contenido = gridView($registros);

require_once "plantilla.php";
