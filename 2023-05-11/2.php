<?php
// creamos una conexion
// creamos un objeto de tipo conexion
$conexion = new mysqli(
    "localhost", // servidor de base de datos
    "root",      // usuario
    "",         // contraseña
    "desarrollo", // base de datos
    3306        // puerto
);

// quiero crear una consulta
// que me permita listar los examenes

$examenesConsulta = $conexion->query("select * from examenes");

// quiero ejecutar la consulta
// fetch_all : todos los registros
// fetch_array : un registro
// fetch_assoc : un registro


$examenes = $examenesConsulta->fetch_all(MYSQLI_ASSOC);

// quiero mostrar todos los registros del array en una tabla html
// foreach (recomendado)
// for
// while - do while

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <table border="1">
        <thead style="background-color:#ccc">
            <tr>
                <td>Id</td>
                <td>Titulo</td>
                <td>Nota</td>
                <td>Fecha</td>
                <td>Codigo del alumno</td>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($examenes as $examen) {
            ?>
                <tr>
                    <td>
                        <?= $examen["id"] ?>
                    </td>
                    <td>
                        <?= $examen["titulo"] ?>
                    </td>
                    <td>
                        <?= $examen["nota"] ?>
                    </td>
                    <td>
                        <?= $examen["fecha"] ?>
                    </td>
                    <td>
                        <?= $examen["codigoAlumno"] ?>
                    </td>
                </tr>
            <?php
            }
            ?>
        </tbody>
    </table>

</body>

</html>