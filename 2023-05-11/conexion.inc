<?php

function conectar()
{
    // creamos una conexion
    // creamos un objeto de tipo conexion
    return new mysqli(
        "localhost", // servidor de base de datos
        "root", // usuario
        "", // contraseña
        "desarrollo", // base de datos
        3306 // puerto
    );
}

function consulta($conexion, $consulta)
{
    return $conexion->query($consulta);
}

function alumnos($conexion)
{
    $alumnosConsulta = $conexion->query("Select * from alumnos");
    return $alumnosConsulta->fetch_all(MYSQLI_ASSOC);
}

function examenes($conexion)
{
    $examenesConsulta = $conexion->query("Select * from examenes");
    return $examenesConsulta->fetch_all(MYSQLI_ASSOC);
}
