<?php
// para cargar un fichero externo en php
// include
// require
// include_once
// require_once

require_once "conexion.inc";

$conexion = conectar();
$alumnos = alumnos($conexion);
$examenes = examenes($conexion);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    require_once "_alumnos.php";
    require_once "_examenes.php";
    ?>
</body>

</html>