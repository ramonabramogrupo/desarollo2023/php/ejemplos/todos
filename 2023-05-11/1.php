<?php
// creamos una conexion
// creamos un objeto de tipo conexion
$conexion = new mysqli(
    "localhost", // servidor de base de datos
    "root",      // usuario
    "",         // contraseña
    "desarrollo", // base de datos
    3306        // puerto
);

// realizar nuestra primera consulta
// quiero sacar los alumnos

$alumnosConsulta = $conexion->query("select * from alumnos");

// leo todos los registros de la tabla
// como array ENUMERADO de arrays ASOCIATIVOS
$alumnos = $alumnosConsulta->fetch_all(MYSQLI_ASSOC);

// quiero mostrar todos los registros del array en una tabla
// foreach (recomendado)
// for
// while - do while

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <table border="1">
        <thead style="background-color:#ccc">
            <tr>
                <td>Codigo</td>
                <td>Nombre</td>
                <td>Correo</td>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($alumnos as $alumno) {
            ?>
                <tr>
                    <td>
                        <?= $alumno["codigo"] ?>
                    </td>
                    <td>
                        <?= $alumno["nombre"] ?>
                    </td>
                    <td>
                        <?= $alumno["correo"] ?>
                    </td>
                </tr>
            <?php
            }
            ?>
        </tbody>
    </table>

</body>

</html>