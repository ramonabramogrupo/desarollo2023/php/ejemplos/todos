 <h2>Alumnos</h2>
 <table border="1">
     <thead style="background-color:#ccc">
         <tr>
             <td>Codigo</td>
             <td>Nombre</td>
             <td>Correo</td>
         </tr>
     </thead>
     <tbody>
         <?php
            foreach ($alumnos as $alumno) {
            ?>
             <tr>
                 <td>
                     <?= $alumno["codigo"] ?>
                 </td>
                 <td>
                     <?= $alumno["nombre"] ?>
                 </td>
                 <td>
                     <?= $alumno["correo"] ?>
                 </td>
             </tr>
         <?php
            }
            ?>
     </tbody>
 </table>