<h2>Examenes</h2>
<table border="1">
    <thead style="background-color:#ccc">
        <tr>
            <td>Id</td>
            <td>Titulo</td>
            <td>Nota</td>
            <td>Fecha</td>
            <td>Codigo del alumno</td>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($examenes as $examen) {
        ?>
            <tr>
                <td>
                    <?= $examen["id"] ?>
                </td>
                <td>
                    <?= $examen["titulo"] ?>
                </td>
                <td>
                    <?= $examen["nota"] ?>
                </td>
                <td>
                    <?= $examen["fecha"] ?>
                </td>
                <td>
                    <?= $examen["codigoAlumno"] ?>
                </td>
            </tr>
        <?php
        }
        ?>
    </tbody>
</table>