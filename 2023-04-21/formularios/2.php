<?php
$numero = 0;

// si pulso el boton de enviar muestro la tabla del numero pedido
if (isset($_GET["enviar"])) {
    $numero = $_GET["numero"];
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form action="">
        <div>
            <label for="numero">Numero</label>
            <input type="number" name="numero" id="numero" required>
        </div>
        <div>
            <button name="enviar">Realizar la tabla de multiplicar</button>
        </div>
    </form>
    <?php
    if ($numero <> 0) {
        // mostrar la tabla de multiplicar del numero
        for ($contador = 1; $contador <= 10; $contador++) {
            echo "<br>{$numero}*{$contador}=" . ($numero * $contador);
        }
    }

    ?>
</body>

</html>