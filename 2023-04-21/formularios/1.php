<?php
// Me muestra en pantalla los numeros del 1 al valor
// introducido en el formulario
// de 1 en 1
$vueltas = 0;

// comprobando si he pulsado el boton de enviar
if (isset($_GET["enviar"])) {
    $vueltas = $_GET["vueltas"]; // leo el valor escrito en el formulario
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form action="">
        <div>
            <label for="vueltas">Vueltas</label>
            <input type="number" id="vueltas" name="vueltas">
        </div>
        <button name="enviar">Mostrar</button>
    </form>
    <?php
    for ($contador = 1; $contador < $vueltas; $contador++) {
        echo "{$contador}<br>";
    }
    ?>

</body>

</html>