<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    // ejemplo con el for
    $numero = 10;

    // mostrar la tabla de multiplicar del numero
    for ($contador = 1; $contador <= 10; $contador++) {
        echo "<br>$numero*$contador=" . ($numero * $contador);
    }

    // mostrar la tabla de multiplicar del numero
    for ($contador = 1; $contador <= 10; $contador++) {
        echo "<br>{$numero}*{$contador}=" . ($numero * $contador);
    }

    // mostrar la tabla de multiplicar del numero
    for ($contador = 1; $contador <= 10; $contador++) {
    ?>
        <br><?= "{$numero}*{$contador}=" . ($numero * $contador) ?>
    <?php
    }
    ?>
</body>

</html>