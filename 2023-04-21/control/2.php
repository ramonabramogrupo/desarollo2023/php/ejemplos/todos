<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        li {
            list-style-type: none;
        }
    </style>
</head>

<body>

    <?php
    // realizar la tabla de multiplicar 
    // de un numero utilizando for
    // llegar hasta un numero maximo
    // la tabla meterla en una lista sin simbolo

    // utilizando el for
    $numero = 2; // calcular la tabla de multiplicar del 2
    $maximo = 20; // debe realizar la tabla hasta 20

    ?>
    <ul>
        <?php
        for ($contador = 1; $contador <= $maximo; $contador++) {
        ?>
            <li>
                <?= "{$numero}*{$contador}=" . ($numero * $contador) ?>
            </li>
        <?php
        }
        ?>
    </ul>

    <?php
    echo "<ul>";
    for ($contador = 1; $contador <= $maximo; $contador++) {
        echo "<li>";
        echo "{$numero}*{$contador}=" . ($numero * $contador);
        echo "</li>";
    }
    echo "</ul>";
    ?>
</body>

</html>