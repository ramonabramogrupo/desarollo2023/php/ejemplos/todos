<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        rect {
            fill: blue;
            stroke-width: 2;
            stroke: black;
        }
    </style>
</head>

<body>
    <?php
    // dibujar un for 10 rectangulos mediante svg
    $numero = 10;

    ?>
    <svg width="800" height="2000">
        <?php
        // inicializo el acumulador
        $acumulador = 10; // indica la coordenada y de la caja

        for ($contador = 0; $contador < $numero; $contador++) {
        ?>
            <rect x="10" y="<?= $acumulador ?>" width="100" height="100" />
        <?php
            $acumulador += 110; // incremento el acumulador
        }
        ?>
    </svg>

</body>

</html>