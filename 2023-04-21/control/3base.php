<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    $numero = 10; // vueltas del bucle
    $acumulador = 12; // es lo que muestro
    $incremento = 100; // lo que sumo en cada vuelta

    for ($contador = 0; $contador < $numero; $contador++) {
        echo "{$acumulador}<br>";
        $acumulador += $incremento;
    }
    // 1 vuelta
    // 12
    // 2 vuelta
    // 112
    // 3 vuelta
    // 212
    // 4 vuelta
    // 312
    // ....
    // 10 vuelta
    // 912




    ?>
</body>

</html>