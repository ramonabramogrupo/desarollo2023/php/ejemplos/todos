<?php
// cargo la libreria
require_once "conexion.inc";

// establecer la conexion con la base de datos
$conexion = conectar("desarrollo");

// tengo que sacar la nota mas alta con el nombre del alumno
$consulta1 = consulta(
    $conexion,
    "SELECT * FROM examenes e JOIN alumnos a ON e.codigoAlumno = a.codigo ORDER BY nota DESC LIMIT 1"
);
$notas = $consulta1->fetch_all(MYSQLI_ASSOC);


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <?php
    require_once "_menu.php";
    ?>
    <h1>Aplicacion de gestion de examenes y alumnos</h1>
    <?php
    require_once "_todo.php";
    ?>

</body>

</html>