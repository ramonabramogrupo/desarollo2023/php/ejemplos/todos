<?php
require_once "utilidades.inc";

// obtener el nombre de la pagina que carga el menu
$pagina = obtenerNombrePagina();

// crear un array para el menu
$menu = [
    "Home" => "index.php",
    "Alumnos"   => "alumnos.php",
    "Examenes" => "examenes.php",
    "Todo" => "todo.php"
];

dibujarMenu($menu, $pagina);
