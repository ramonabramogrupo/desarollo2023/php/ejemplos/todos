<?php
// cargo la libreria
require_once "conexion.inc";

// establecer la conexion con la base de datos
$conexion = conectar("desarrollo");

// obtengo los alumnos de la bbdd
$alumnos = alumnos($conexion);

//obtengo los examenes
$examenes = examenes($conexion);

// obtengo todo
$notas = todo($conexion);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <?php
    require_once "_menu.php";
    ?>
    <h1>Todo</h1>
    <?php
    require_once "_alumnos.php";
    require_once "_examenes.php";
    require_once "_todo.php";
    ?>

</body>

</html>