<?php


/**
 * funcion que permite conectarse a una base de datos
 *
 * @param  string $baseDatos nombre de la base de datos
 * @return object Conexion a la base de datos
 */
function conectar($baseDatos)
{
    // creamos una conexion
    // creamos un objeto de tipo conexion
    return new mysqli(
        "localhost", // servidor de base de datos
        "root", // usuario
        "", // contraseña
        $baseDatos, // base de datos
        3306 // puerto
    );
}


/**
 * Me permite ejecutar cualquier consulta de sql
 *
 * @param  object $conexion objeto que apunta a la conexion
 * @param  string $consulta consulta a ejecutar
 * @return object objeto de tipo mysqli_result
 */
function consulta($conexion, $consulta)
{
    return $conexion->query($consulta);
}


/**
 * Me permite ejecutar la consulta que devuelve todos los alumnos
 *
 * @param  object $conexion objeto de tipo conexion
 * @return array es un array bidimensional(enumerado y dentro asociativo) con todos los alumnos
 */
function alumnos($conexion)
{
    $alumnosConsulta = $conexion->query("Select * from alumnos");
    return $alumnosConsulta->fetch_all(MYSQLI_ASSOC);
}

/**
 * Me permite realizar un listado de todos los examenes
 *
 * @param  object $conexion objeto de tipo conexion
 * @return array es un array bidimensional(enumerado y dentro asociativo) con todos los examenes
 */
function examenes($conexion)
{
    $examenesConsulta = $conexion->query("Select * from examenes");
    return $examenesConsulta->fetch_all(MYSQLI_ASSOC);
}

function todo($conexion)
{
    $consulta = $conexion->query("SELECT * FROM examenes e JOIN alumnos a ON e.codigoAlumno = a.codigo ORDER BY nota DESC");
    return $consulta->fetch_all(MYSQLI_ASSOC);
}
