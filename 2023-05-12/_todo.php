<?php
// necesito un array llamado notas 
// array $notas[]
// esta vista me muestra todos los examenes con los datos del alumno

?>
<table border="1">
    <thead style="background-color:#ccc">
        <tr>
            <td>Id</td>
            <td>Titulo</td>
            <td>Nota</td>
            <td>Fecha</td>
            <td>Codigo del alumno</td>
            <td>Nombre del alumno</td>
            <td>Correo del alumno</td>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($notas as $nota) {
        ?>
            <tr>
                <td>
                    <?= $nota["id"] ?>
                </td>
                <td>
                    <?= $nota["titulo"] ?>
                </td>
                <td>
                    <?= $nota["nota"] ?>
                </td>
                <td>
                    <?= $nota["fecha"] ?>
                </td>
                <td>
                    <?= $nota["codigoAlumno"] ?>
                </td>
                <td>
                    <?= $nota["nombre"] ?>
                </td>
                <td>
                    <?= $nota["correo"] ?>
                </td>
            </tr>
        <?php
        }
        ?>
    </tbody>
</table>