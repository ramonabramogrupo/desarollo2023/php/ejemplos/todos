<?php
$ancho = 5; // variable que controla el ancho de los bordes
$radio = 25; // variable que controla el radio de los bordes

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .cajas {
            border: <?= $ancho ?>px solid black;
            background-color: #999;
        }

        .redondeado {
            border-radius: <?= $radio ?>px;
        }
    </style>
</head>

<body>
    <div class="cajas redondeado">Ramon</div>
    <div class="cajas redondeado">Alpe</div>
    <div class="cajas redondeado">Desarrollo</div>
</body>

</html>