<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    $nombre = "";

    // comprobar si tengo que cargar el formulario o el resultado
    // if(!$_GET)
    if (!isset($_GET["enviar"])) { // si no he pulsado enviar

    ?>
        <form action="">
            <div>
                <label for="nombre">Nombre</label>
                <input type="text" id="nombre" name="nombre">
            </div>
            <div>
                <button name="enviar">Enviar</button>
            </div>
        </form>
    <?php
    } else {
        // aqui tengo que colocar los resultados
        //porque entra cuando he pulsado el boton de enviar

        $nombre = $_GET["nombre"];
    }
    ?>
    <div>
        Nombre: <?= $nombre ?>
    </div>
</body>

</html>