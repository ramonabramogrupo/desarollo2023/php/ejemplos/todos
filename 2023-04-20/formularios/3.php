<?php
// valores por defecto para las variables
$ancho = 2;
$radio = 20;
// si he pulsado el boton de enviar 
// leo el formulario
if (isset($_GET["enviar"])) {
    $ancho = $_GET["ancho"];
    $radio = $_GET["radio"];
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .cajas {
            border: <?= $ancho ?>px solid black;
            background-color: #999;
        }

        .redondeado {
            border-radius: <?= $radio ?>px;
        }
    </style>
</head>

<body>
    <form action="">
        <div>
            <label for="ancho">Ancho borde</label>
            <input type="number" id="ancho" name="ancho">
        </div>
        <div>
            <label for="radio">Radio del borde</label>
            <input type="number" id="radio" name="radio">
        </div>
        <div>
            <button name="enviar">Cambiar</button>
        </div>
    </form>

    <div class="cajas redondeado">Ramon</div>
    <div class="cajas redondeado">Alpe</div>
    <div class="cajas redondeado">Desarrollo</div>
</body>

</html>