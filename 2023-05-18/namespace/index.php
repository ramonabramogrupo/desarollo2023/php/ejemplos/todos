<?php
// autocarga de clases
// cada vez que instancie un objeto (new ...)
// llama a esta funcion
// como utilizo espacios de nombres no coloco los directorios delante de la clase

/**
 *  me permite realizar la autocarga de clases
 */
spl_autoload_register(function ($nombreClase) {
    require_once "$nombreClase.php";
});

// mediante el use conseguimos que la clase
// este en el espacio de nombres de actual

use clases\uno\Caja;

// me carga la clase Caja del espacio de nombres dos 
// renombrado la clase
use clases\dos\Caja as DosCaja;

// precargando la clase Lista en mi espacio de nombres
use clases\uno\Lista;

// instanciar una clase
// creo un objeto de tipo caja
$titulo = new Caja();

// esto es como si colocase sin el use
// $titulo=new clases\uno\Caja();

var_dump($titulo);

// quiero crear una caja de tipo dos
$titulo1 = new DosCaja();

var_dump($titulo1);

// quiero crear un objeto de tipo lista

$listado = new Lista();

var_dump($listado);
