<div style="background-color: #ccc;margin:20px auto;width:900px;padding:20px">
    <h2>Crear clase y objeto</h2>

    <p>La clase debe tener las siguientes caracteristicas</p>
    <ul>
        <li>Propiedades
            <ul>
                <li>nombre</li>
                <li>apellidos</li>
                <li>edad</li>
            </ul>
        </li>
        <li>Metodos
            <ul>
                <li>saludar</li>
                <li>presentacion</li>
            </ul>
        </li>
    </ul>
</div>

<?php

// creamos una clase 
class Alumno
{
    // propiedades de la clase

    // visibilidad nombrePropiedad
    public $nombre = "Ana";
    public $apellidos = "Vazquez";
    public $edad = 40;

    // metodos de la clase

    // visibilidad function nombreMetodo(argumentos)
    public function saludar()
    {
        return "Hola clase<br>";
    }

    public function presentacion()
    {
        return "hola mi nombre es {$this->nombre} y mis apellidos son {$this->apellidos}<br>";
    }
}

?>

<?php

// para poder utilizar la clase
// tengo que generar una instancia

// creamos un objeto de tipo Alumno
$alumno1 = new Alumno();

// accediendo a la propiedad nombre del objeto
echo "El nombre del alumno es {$alumno1->nombre} y mis apellidos son {$alumno1->apellidos}<br>";

// acceder al metodo saludar
// para mostrar en pantalla el saludo
echo $alumno1->saludar();

// acceder al metodo presentacion
// para mostrarlo en pantalla

echo $alumno1->presentacion();
