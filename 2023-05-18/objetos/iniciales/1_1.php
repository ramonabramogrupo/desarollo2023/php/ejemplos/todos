<div style="background-color: #ccc;margin:20px auto;width:900px;padding:20px">
    <h2>Hemos utilizado metodo magico constructor</h2>
</div>

<?php

// creamos una clase 

use Alumno as GlobalAlumno;

class Alumno
{
    // propiedades de la clase

    // visibilidad nombrePropiedad
    public $nombre;
    public $apellidos;
    public $edad;

    // metodos magicos de la clase
    // son metodos que se ejecutan automaticamente
    // ante determinadas condiciones
    public function __construct($nombre, $apellidos, $edad)
    {
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->edad = $edad;
    }


    // metodos de la clase

    // visibilidad function nombreMetodo(argumentos)
    public function saludar()
    {
        return "Hola clase<br>";
    }

    public function presentacion()
    {
        return "hola mi nombre es {$this->nombre} y mis apellidos son {$this->apellidos}<br>";
    }
}
?>

<?php
// para poder utilizar la clase
// tengo que generar una instancia

// creamos un objeto de tipo Alumno
$alumno1 = new Alumno("Ana", "Vazquez", 40); // cuando creo el objeto se llama al constructor

// acceder al metodo presentacion
// para mostrarlo en pantalla

echo $alumno1->presentacion();

var_dump($alumno1);

// crear otro objeto alumno
$alumno2 = new Alumno(
    "Silvia",
    "Abascal",
    "19"
);

// quiero que se presente el segundo alumno
// llamar a un metodo
echo $alumno2->presentacion();

// quiero cambiar el nombre de silvia
// modificando una propiedad (esto sera el objetivo del setter)
$alumno2->nombre = "Silvia Manuela";

// mostrar el nuevo nombre
// leer el valor de una propiedad (esto sera el trabajo del getter)
echo $alumno2->nombre;
