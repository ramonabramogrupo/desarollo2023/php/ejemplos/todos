<div style="background-color: #ccc;margin:20px auto;width:900px;padding:20px">
    <h2>Añadimos getter y setter</h2>
</div>

<?php

// creamos una clase 
class Alumno
{
    // propiedades de la clase

    // visibilidad nombrePropiedad
    public $nombre;
    public $apellidos;
    public $edad;

    // metodos magicos de la clase
    // son metodos que se ejecutan automaticamente
    // ante determinadas condiciones
    public function __construct($nombre, $apellidos, $edad)
    {
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->edad = $edad;
    }


    // metodos getters y setters
    public function getNombre()
    {
        return $this->nombre;
    }

    public function getApellidos()
    {
        return $this->apellidos;
    }

    public function getEdad()
    {
        return $this->edad;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
        return $this; // para que sea fluent
    }

    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;
        return $this; // fluent
    }

    public function setEdad($edad)
    {
        $this->edad = $edad;
        return $this; // fluent
    }

    // metodos de la clase

    // visibilidad function nombreMetodo(argumentos)
    public function saludar()
    {
        return "Hola clase<br>";
    }

    public function presentacion()
    {
        return "hola mi nombre es {$this->nombre} y mis apellidos son {$this->apellidos}<br>";
    }
}

?>

<?php

// para poder utilizar la clase
// tengo que generar una instancia

// creamos un objeto de tipo Alumno
// cuando creo el objeto se llama al constructor
$alumno1 = new Alumno("Ana", "Vazquez", 40);

// acceder al metodo presentacion
// para mostrarlo en pantalla

echo $alumno1->presentacion();

var_dump($alumno1);

// cambiar el nombre y la edad con setter
// sin fluent
$alumno1->setNombre("Luis");
$alumno1->setEdad(41);

// cambiando el nombre sin setter
$alumno1->nombre = "Luis";
// cambiando la edad sin setter
$alumno1->edad = 41;

// mostrar los datos del alumno1
echo $alumno1->presentacion();

// modificar el nombre y la edad con setter
// con fluent
$alumno1->setNombre("Luisa")->setEdad(60);

var_dump($alumno1);

// al intentar mostrar el objeto 
// llama al metodo toString
// echo $alumno1; // me da error porque no tengo toString
