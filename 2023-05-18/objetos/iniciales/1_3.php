<div style="background-color: #ccc;margin:20px auto;width:900px;padding:20px">
    <h2>Añadir metodo magico destructor</h2>
</div>

<?php

// creamos una clase 
class Alumno
{
    // propiedades de la clase

    // visibilidad nombrePropiedad
    public $nombre;
    public $apellidos;
    public $edad;

    // metodos magicos de la clase
    // son metodos que se ejecutan automaticamente
    // ante determinadas condiciones
    public function __construct($nombre, $apellidos, $edad)
    {
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->edad = $edad;
    }

    // metodo magico que se llama cuando se elimina el objeto
    // con la funcion unset por ejemplo
    public function __destruct()
    {
        echo "Destruido";
    }

    // este metodo magico se llamara cuando intente
    // imprimir un objeto

    public function __toString()
    {
        return "{$this->nombre},{$this->apellidos},{$this->edad}";
    }


    // metodos getters y setters
    public function getNombre()
    {
        return $this->nombre;
    }

    public function getApellidos()
    {
        return $this->apellidos;
    }

    public function getEdad()
    {
        return $this->edad;
    }

    public function setNombre($nombre)
    {
        // reglas
        $this->nombre = $nombre;
        return $this; // para que sea fluent
    }

    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;
        return $this;
    }

    public function setEdad($edad)
    {
        $this->edad = $edad;
        return $this;
    }

    // metodos de la clase

    // visibilidad function nombreMetodo(argumentos)
    public function saludar()
    {
        return "Hola clase<br>";
    }

    public function presentacion()
    {
        return "hola mi nombre es {$this->nombre} y mis apellidos son {$this->apellidos}<br>";
    }
}


?>

<?php
// para poder utilizar la clase
// tengo que generar una instancia

// creamos un objeto de tipo Alumno
// cuando creo el objeto se llama al constructor
$alumno1 = new Alumno("Ana", "Vazquez", 40);

// acceder al metodo presentacion
// para mostrarlo en pantalla

echo $alumno1->presentacion();

var_dump($alumno1); // estamos depurando el objeto alumno1

// cambiar el nombre y la edad
// sin fluent
$alumno1->setNombre("Luis"); // $alumno1->nombre="Luis";
$alumno1->setEdad(41); // $alumno1->edad=41

var_dump($alumno1);

// con fluent
$alumno1->setNombre("Luisa")->setEdad(45);


echo $alumno1->presentacion();

echo $alumno1; // al intentar imprimir el alumno llama a __toString

// al final del codigo se llama a los destructores