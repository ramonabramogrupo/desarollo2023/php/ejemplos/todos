<div style="background-color: #ccc;margin:20px auto;width:900px;padding:20px">
    <h2>crear una clase denomina coche con las siguientes propiedades</h2>
    <li>tipo</li>
    <li>matricula</li>
    <li>cilindrada</li>
    <li>fechaCompra</li>

    <p>Crear los getter y setter para todas las propiedades (los setter fluent)</p>

    <p>Constructor para inicializar solo matricula</p>

    <p>Metodo toString para imprimir matricula y tipo en una lista</p>

</div>

<?php

// crear una clase denomina coche
// con las siguientes propiedades
// tipo
// matricula
// cilindrada
// fechaCompra
// Crear los getter y setter para todas las propiedades
// los setter fluent
// constructor para inicializar solo matricula
// metodo toString para imprimir matricula y tipo en una lista

class Coche
{

    // propiedades
    // visibilidad (public, private, protected)
    public $tipo;
    public $matricula;
    public $cilindrada;
    public $fechaCompra;

    // metodo magico constructor
    public function __construct($matricula)
    {
        $this->matricula = $matricula;
    }

    public function __toString()
    {
        $salida = "<ul>";
        $salida .= "<li>{$this->matricula}</li>";
        $salida .= "<li>{$this->tipo}</li>";
        $salida .= "</ul>";
        return $salida;
    }

    // getters
    public function getTipo()
    {
        return $this->tipo;
    }

    public function getMatricula()
    {
        return $this->matricula;
    }

    public function getCilindrada()
    {
        return $this->cilindrada;
    }

    public function getFechaCompra()
    {
        return $this->fechaCompra;
    }

    // setters

    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
        return $this;
    }

    public function setMatricula($matricula)
    {
        $this->matricula = $matricula;
        return $this;
    }

    public function setCilindrada($cilindrada)
    {
        $this->cilindrada = $cilindrada;
        return $this;
    }

    /**
     * Set the value of fechaCompra
     *
     * @return  self
     */
    public function setFechaCompra($fechaCompra)
    {
        $this->fechaCompra = $fechaCompra;

        return $this;
    }
}

?>

<?php
// probar mi clase
$coche1 = new Coche("3421DFG");

echo $coche1; // imprimo el coche1 (toString)

var_dump($coche1); // depurando el objeto
// inicilizar el tipo
$coche1->tipo = "Turismo";  // propiedad

// inicializando el tipo desde su setter
$coche1->setTipo("Turismo"); //setter


$coche1->setFechaCompra("2022/05/01");  // coche1->fechaCompra="2022/05/01";
// modificar la matricula
$coche1->setMatricula("1111AAA");  // $coche1->matricula="1111AAA";

var_dump($coche1);
