<?php

namespace clases\objetos;

use clases\animales\Persona;
use clases\animales\Empleado;

class Cuenta {

    public $persona; //objeto persona
    public $empleado; //objeto empleado
    private $saldo;
    private $activa;
    public static $numero = 0;

    public function __construct($persona, $empleado, $saldo, $activa) {
        $this->persona = clone $persona;
        $this->empleado = clone $empleado;
        $this->saldo = $saldo;
        $this->activa = $activa;
    }

}
