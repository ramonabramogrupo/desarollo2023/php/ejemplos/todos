<?php

/*
 * Crear la clase Persona con las siguientes caracteristicas
 *
 * Propiedades publicas
 * nombre
 * apellidos
 * edad
 *
 * Getter y setter fluent
 *
 * constructor que inicializa todo
 *
 * destructor que muestra el texto destruido en la consola de javascript
 *
 * metodo toString que devuelve el nombre, los apellidos y la edad separados por comas
 *
 */

namespace clases\animales;

class Persona {

    // propiedades de la clase
    // visibilidad nombrePropiedad
    public $nombre;
    public $apellidos;
    public $edad;

    // metodos magicos de la clase
    // son metodos que se ejecutan automaticamente
    // ante determinadas condiciones
    public function __construct($nombre, $apellidos, $edad) {
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->edad = $edad;
        echo "hola";
    }

    // metodo magico que se llama cuando se elimina el objeto
    // con la funcion unset por ejemplo
    public function __destruct() {
        echo "Destruido";
    }

    // este metodo magico se llamara cuando intente
    // imprimir un objeto

    public function __toString() {
        return "{$this->nombre},{$this->apellidos},{$this->edad}";
    }

    // metodos getters y setters
    public function getNombre() {
        return $this->nombre;
    }

    public function getApellidos() {
        return $this->apellidos;
    }

    public function getEdad() {
        return $this->edad;
    }

    public function setNombre($nombre) {
        // reglas
        $this->nombre = $nombre;
        return $this; // para que sea fluent
    }

    public function setApellidos($apellidos) {
        $this->apellidos = $apellidos;
        return $this;
    }

    public function setEdad($edad) {
        $this->edad = $edad;
        return $this;
    }

}
