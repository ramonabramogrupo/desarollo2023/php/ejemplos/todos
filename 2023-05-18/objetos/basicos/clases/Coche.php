<?php

/**
 * Description of Coche
 *
 * @author Profesor Ramon
 */
class Coche
{
    // propiedades
    // visibilidad (public, private, protected)
    public $tipo;
    private $matricula;  // la matricula es privada
    public $cilindrada;
    public $fechaCompra;


    // metodo magico constructor
    public function __construct($matricula)
    {
        $this->matricula = $matricula;
    }

    public function __toString()
    {
        $salida = "<ul>";
        $salida .= "<li>{$this->matricula}</li>";
        $salida .= "<li>{$this->tipo}</li>";
        $salida .= "</ul>";
        return $salida;
    }

    // getters
    public function getTipo()
    {
        return $this->tipo;
    }

    public function getMatricula()
    {
        return $this->matricula;
    }

    public function getCilindrada()
    {
        return $this->cilindrada;
    }

    public function getFechaCompra()
    {
        return $this->fechaCompra;
    }

    // setters

    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
        return $this;
    }

    public function setMatricula($matricula)
    {
        $this->matricula = $matricula;
        return $this;
    }

    public function setCilindrada($cilindrada)
    {
        $this->cilindrada = $cilindrada;
        return $this;
    }

    public function setFechaCompra($fechaCompra)
    {
        $this->fechaCompra = $fechaCompra;
        return $this;
    }
}
