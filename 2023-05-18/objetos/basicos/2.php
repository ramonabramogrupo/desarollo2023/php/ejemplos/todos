<div style="background-color: #ccc;margin:20px auto;width:900px;padding:20px">
    <h2>Vamos a utilizar una clase denominada Coche</h2>

    <p>La cargamos mediante require_once</p>
</div>

<div style="background-color: #ccc;margin:20px auto;width:900px;padding:20px">
    <pre>
        // cargamos la clase o clases
        // que vayamos a utilizar


        // quiero crear un objeto de tipo coche



       // muestra informacion sobre el coche creado con var_dump
    </pre>
</div>


<div style="background-color: #ccc;margin:20px auto;width:900px;padding:20px">
    <pre>
        // cargamos la clase o clases
        // que vayamos a utilizar
        require_once './clases/Coche.php';

        // quiero crear un objeto de tipo coche

        $coche1 = new Coche("2323AC");

        // muestra informacion sobre el coche creado con var_dump
        var_dump($coche1);
    </pre>
</div>

<?php
// cargamos la clase o clases
// que vayamos a utilizar
require_once './clases/Coche.php';

// quiero crear un objeto de tipo coche

$coche1 = new Coche("2323AC");

// muestra informacion sobre el coche creado con var_dump
var_dump($coche1);
