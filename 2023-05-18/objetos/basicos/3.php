<?php
spl_autoload_register(function ($nombreClase) {
    require_once "$nombreClase.php";
});
?>
<div style="background-color: #ccc;margin:20px auto;width:900px;padding:20px">
    <h2>Vamos a comenzar con algo de herencia</h2>
    <p>Necesitamos las clases
    <ul>
        <li>Persona: en el espacio clases/animales</li>
        <li>Empleado: en el espacio clases/animales</li>
        <li>El empleado hereda de Persona</li>
    </ul>

</div>

<div style="background-color: #ccc;margin:20px auto;width:900px;padding:20px">
    <pre>
        // importo las clases


        // crear una persona

        // muestro informacion sobre la persona con var_dump


        //muestro informacion de la persona con echo


        // creo un empleado

        // muestro informacion sobre el empleado con var_dump


        // muestro empleado con echo

        // presento al empleado con presentar

    </pre>

</div>

<div style="background-color: #ccc;margin:20px auto;width:900px;padding:20px">
    <pre>
        // importo las clases
        use clases\animales\Persona;
        use clases\animales\Empleado;

        // crear una persona
        $persona1 = new Persona("Ramon", "Abramo", 50);
        // muestro informacion sobre la persona con var_dump
        var_dump($persona1);

        //muestro informacion de la persona con echo
        echo $persona1;

        // creo un empleado
        $empleado1 = new Empleado(1000, "alpe", "Jose", "Vazquez", 26);

        // muestro informacion sobre el empleado con var_dump
        var_dump($empleado1);

        // muestro empleado con echo
        echo $empleado1;

        // presento al empleado con presentar
        echo $empleado1->presentar();
    </pre>

</div>


<?php

// importo las clases
use clases\animales\Persona;
use clases\animales\Empleado;

// crear una persona
$persona1 = new Persona("Ramon", "Abramo", 50);
// muestro informacion sobre la persona con var_dump
var_dump($persona1);

//muestro informacion de la persona con echo
echo $persona1;

// creo un empleado
$empleado1 = new Empleado(1000, "alpe", "Jose", "Vazquez", 26);

// muestro informacion sobre el empleado con var_dump
var_dump($empleado1);

// muestro empleado con echo
echo $empleado1;

// presento al empleado con presentar
echo $empleado1->presentar();

