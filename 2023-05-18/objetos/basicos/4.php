<?php

use clases\animales\Empleado;
use clases\animales\Persona;

spl_autoload_register(function ($nombreClase) {
    require_once "$nombreClase.php";
});
?>

<div style="background-color: #ccc;margin:20px auto;width:900px;padding:20px">
    <h2>Vamos a comenzar con algo de herencia</h2>
    <p>Necesitamos las clases
    <ul>
        <li>Persona: en el espacio clases/animales</li>
        <li>Empleado: en el espacio clases/animales</li>
        <li>El empleado hereda de Persona</li>
    </ul>

</div>

<?php

$persona1 = new Persona(
    "Eva",
    "Vazquez",
    18
);

var_dump($persona1);

$empleado1 = new Empleado(
    1000,
    "alpe",
    "Jose",
    "Lopez",
    20
);

var_dump($empleado1);
