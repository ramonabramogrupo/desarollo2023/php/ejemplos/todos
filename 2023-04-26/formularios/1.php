<?php

$vocales = [
    "a" => 0,
    "e" => 0,
    "i" => 0,
    "o" => 0,
    "u" => 0
];

$texto = "";
$textoOriginal = "";

if (isset($_GET["enviar"])) {
    // leer el texto de la caja del formulario
    $textoOriginal = $_GET["texto"];

    // convertir el texto a minusculas
    $texto = strtolower($textoOriginal);

    //procesamiento de la informacion

    // recorrer el texto y comprobar si es una vocal
    for ($indice = 0; $indice < strlen($texto); $indice++) {
        // leer cada caracter
        $caracter = $texto[$indice];

        // comprobando si es una vocal
        if (
            $caracter == 'a' ||
            $caracter == 'e' ||
            $caracter == 'i' ||
            $caracter == 'o' ||
            $caracter == 'u'
        ) {
            // incremento en uno, la posicion del array
            // que indica el caracter
            $vocales[$caracter]++;
        }
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form action="">
        <div>
            <label for="texto">Introducir texto</label>
            <input type="text" id="texto" name="texto">
        </div>
        <div>
            <button name="enviar">
                Calcular
            </button>
        </div>
    </form>

    <?php
    echo "{$textoOriginal}<br>";
    foreach ($vocales as $indice => $valor) {
        echo "{$indice}={$valor}<br>";
    }
    ?>

</body>

</html>