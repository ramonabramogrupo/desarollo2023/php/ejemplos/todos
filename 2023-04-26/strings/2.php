<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    $texto = "ejemplo de clase"; // string

    // partir un string en array
    // explode
    $vector = explode(" ", $texto); // array con los palabras del string
    var_dump($vector);

    // juntar un array en un string
    // implode (join)
    $final = implode("-", $vector);
    echo $final;

    // convertir un string en un array
    $vector = str_split($texto);
    var_dump($vector);

    ?>
</body>

</html>