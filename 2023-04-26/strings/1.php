<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    $texto = "hola clase";

    echo "{$texto}<br>"; // muestro todo el string
    echo "El segundo caracter es una {$texto[1]}<br>";

    // quiero recorrer el string
    for ($indice = 0; $indice < strlen($texto); $indice++) {
        $caracter = $texto[$indice];
        echo "{$indice}={$caracter}<br>";
    }



    ?>
</body>

</html>