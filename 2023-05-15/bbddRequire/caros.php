<?php
require_once "librerias/conexion.inc";
require_once "librerias/utilidades.inc";

$conexion = conectar("tienda");

$resultado = consultaArray(
    $conexion,
    "SELECT 
                p.codigo,
                p.nombre,
                p.precio
            FROM 
                producto p 
            ORDER BY 
                precio DESC
            LIMIT 3;"
);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <?php
    require_once "_menu.php";

    gridView($resultado);

    ?>
</body>

</html>