<?php
// cargo la libreria
require_once "librerias/conexion.inc";

// establecer la conexion con la base de datos
$conexion = conectar("tienda");

// saco los fabricantes
$fabricantes = consultaArray($conexion, "Select * from fabricante");

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <?php
    require_once "_menu.php";
    ?>
    <h1>Listado de Fabricantes</h1>

    <?php

    gridView(
        $fabricantes, // registros
        ["codigo", "nombre"] // campos
    );
    ?>
</body>

</html>