<?php
require_once "librerias/utilidades.inc";

// obtener el nombre de la pagina que carga el menu
$pagina = obtenerNombrePagina();

// crear un array para el menu
$menu = [
    "Home" => "index.php",
    "Fabricantes"   => "fabricante.php",
    "Productos" => "producto.php",
    "Listado completo" => "todo.php"
];

dibujarMenu($menu, $pagina);
