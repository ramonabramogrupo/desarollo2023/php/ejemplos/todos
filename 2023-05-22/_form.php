<form method="get">
    <div>
        <label for="bastidor">Bastidor</label>
        <input type="text" name="bastidor" id="bastidor" value="<?= $coche->bastidor ?>">
    </div>
    <div>
        <label for="marca">Marca</label>
        <input type="text" name="marca" id="marca" value="<?= $coche->marca ?>">
    </div>
    <div>
        <label for="modelo">Modelo</label>
        <input type="text" name="modelo" id="modelo" value="<?= $coche->modelo ?>">
    </div>
    <div>
        <label for="cilindrada">Cilindrada</label>
        <input type="text" name="cilindrada" id="cilindrada" value="<?= $coche->cilindrada ?>">
    </div>
    <div>
        <button name="enviar">Enviar</button>
        <button type="reset">Limpiar</button>
    </div>
</form>