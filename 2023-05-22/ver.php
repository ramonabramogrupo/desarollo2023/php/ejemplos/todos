<?php
spl_autoload_register(function ($nombreClase) {
    require_once "$nombreClase.php";
});

use clases\librerias\Conexion;
use clases\elementos\Coche;

$conexion = new Conexion([
    "basededatos" => "concesionario",
]);

// tengo los datos del coche en un array
$datos=$conexion->consulta(
    "select * from coche where bastidor='{$_GET["bastidor"]}'"
)->obtenerDatos()[0];

// tengo los datos del coche en un modelo (objeto)
$coche = new Coche([
    "bastidor" => $datos["bastidor"],
    "marca" => $datos["marca"],
    "modelo" => $datos["modelo"],
    "cilindrada" => $datos["cilindrada"]
]);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
    <?php
    require_once "_menu.php";
    //echo $datos["bastidor"];
    echo $coche->dibujarTabla();
    ?>
</body>
</html>