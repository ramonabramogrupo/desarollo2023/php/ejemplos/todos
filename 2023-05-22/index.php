<?php

use clases\elementos\Coche;
use clases\librerias\Conexion;
use clases\librerias\Utilidades;

spl_autoload_register(function ($nombreClase) {
    require_once "$nombreClase.php";
});

$conexion1 = new Conexion([
    "basededatos" => "concesionario",
]);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <?php
    require_once "_menu.php";
    ?>
    <div>
        
    <a href="insertar.php">Añadir registro</a>
    </div>
    <?php

    // consulta para listar
    $datos = $conexion1
        ->consulta("select * from coche")
        ->obtenerDatos();

    echo Utilidades::gridView($datos, [], "bastidor");

    // // actualizar el primer coche
    // $coche1 = new Coche($datos[0]);

    // $coche1->cilindrada++;
    // $coche1->actualizar($conexion1);



    // $datos = $conexion1
    //     ->consulta("select * from coche")
    //     ->obtenerDatos();


    ?>
</body>

</html>