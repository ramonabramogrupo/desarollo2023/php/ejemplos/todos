<?php

spl_autoload_register(function ($nombreClase) {
    require_once "$nombreClase.php";
});

use clases\librerias\Conexion;
use clases\elementos\Coche;

$conexion1 = new Conexion([
    "basededatos" => "concesionario",
]);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/main.css">
</head>

<body>

    <?php
    require_once "_menu.php";
    // he pulsado el boton de enviar
    if (isset($_GET["enviar"])) {

        // crear una variable con cada campo del formulario
        extract($_GET, EXTR_PREFIX_SAME, "form");
        // lo mismo pero a mano
        //$bastidor = $_GET["bastidor"];
        //$marca = $_GET["marca"];

        // consulta para insertar
        // $conexion1->consulta("
        //     INSERT INTO coche VALUES ('{$bastidor}', '{$marca}', '{$modelo}',{$cilindrada})
        //     ");

        // // crear un coche nuevo
        $coche = new Coche([
            "bastidor" => $bastidor,
            "marca" => $marca,
            "modelo" => $modelo,
            "cilindrada" => $cilindrada

        ]);

        $coche->insertar($conexion1);

        // redireccionar la pagina a index
        header("Location: index.php");
    } else {

        $coche = new Coche();
        // cargar el formulario
        require_once "_form.php";
    }

    ?>
</body>

</html>