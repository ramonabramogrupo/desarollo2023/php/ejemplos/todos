<?php
session_start(); // para que funcionen las sesiones

use clases\elementos\Coche;
use clases\librerias\Conexion;

spl_autoload_register(function ($nombreClase) {
    require_once "$nombreClase.php";
});
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/main.css">
    <title>Document</title>
</head>

<body>
    <?php
    require_once "_menu.php";

    $conexion = new Conexion([
        "basededatos" => "concesionario"
    ]);

    // he pulsado el boton de actualizar
    if (isset($_GET["enviar"])) {

        $coche = new Coche([
            "bastidor" => $_GET["bastidor"], // bastidor cambiado en el formulario
            "marca" => $_GET["marca"],
            "modelo" => $_GET["modelo"],
            "cilindrada" => $_GET["cilindrada"],
            "bastidorOld" => $_SESSION["bastidor"] // bastidor que esta en la bbdd
        ]);

        $coche->actualizar($conexion);

        header("Location: index.php");
    } else {

        // leo el bastidor del coche que quiero modificar los datos
        // y lo almaceno en una variables session
        $_SESSION["bastidor"] = $_GET["bastidor"];

        // recuperando los datos del coche a modificar de la bbddd
        $datos = $conexion
            ->consulta("
        select * from coche where bastidor='{$_SESSION["bastidor"]}'
        ")
            ->obtenerDatos();

        $coche = new Coche([
            "bastidor" => $datos[0]["bastidor"],
            "marca" => $datos[0]["marca"],
            "modelo" => $datos[0]["modelo"],
            "cilindrada" => $datos[0]["cilindrada"]
        ]);

        require_once("_form.php");
    }
    ?>
</body>

</html>