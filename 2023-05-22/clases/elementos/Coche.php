<?php

namespace clases\elementos;

class Coche
{
    public $marca;
    public $modelo;
    public $cilindrada;
    public $bastidor;
    private $bastidorOld;

    /**
     * __construct
     *
     * @param  array $datos debes pasar un array asociativo con las propiedades a inicializar
     * son marca, modelo, cilindrada, bastidor
     * @return void
     */
    public function __construct($datos = [])
    {
        $this->marca = $datos["marca"] ?? "";
        $this->modelo = $datos["modelo"] ?? "";
        $this->cilindrada = $datos["cilindrada"] ?? 0;
        $this->bastidor = $datos["bastidor"] ?? "";
        $this->bastidorOld = $datos["bastidorOld"] ?? "";
    }

    /*
    metodo magico que se llama cuando intente
    imprimir el objeto 
    */
    public function __toString()
    {
        return "{$this->marca}-{$this->modelo}";
    }

    /**
     * Get the value of bastidor
     */
    public function getBastidor()
    {
        return $this->bastidor;
    }

    /**
     * Set the value of bastidor
     *
     * @return  self
     */
    public function setBastidor($bastidor)
    {
        // regla de negocio (restriccion, rules)
        if (!empty($bastidor)) {
            $this->bastidor = $bastidor;
        }
        return $this;
    }

    public function dibujarTabla()
    {
        ob_start();
?>
        <table border="1">
            <thead>
                <tr>
                    <td>
                        Marca
                    </td>
                    <td>
                        Modelo
                    </td>
                    <td>
                        bastidor
                    </td>
                    <td>
                        Cilindrada
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?= $this->marca ?></td>
                    <td><?= $this->modelo ?></td>
                    <td><?= $this->bastidor ?></td>
                    <td><?= $this->cilindrada ?></td>
                </tr>
            </tbody>
        </table>
<?php
        return ob_get_clean();
    }

    public function insertar($conexion)
    {
        $conexion->consulta("
        insert into coche values('$this->bastidor','$this->marca','$this->modelo',$this->cilindrada)
        ");
    }

    public function actualizar($conexion)
    {
        $conexion->consulta("
        update coche 
            set marca='$this->marca', 
            modelo='$this->modelo', 
            cilindrada=$this->cilindrada,
            bastidor='$this->bastidor'
            where bastidor='$this->bastidorOld' 
        ");
    }

    public function eliminar($conexion){

        $conexion->consulta("
            DELETE FROM coche WHERE bastidor='{$this->bastidor}'
        ");

    }
}
