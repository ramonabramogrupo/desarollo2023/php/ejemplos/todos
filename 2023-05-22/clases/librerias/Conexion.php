<?php

namespace clases\librerias;

use mysqli;

class Conexion
{
    private $conexion;
    private $consulta;

    /**
     * __construct crea una conexion con un servidor de bbdd
     *
     * @param  array $propiedadesConexion debes pasar los parametros como un array asociativo
     * Las claves a inicializar son equipo, usuario, contrasena, basededatos, puerto
     * @return void
     */
    public function __construct($propiedadesConexion)
    {
        /*if(!array_key_exists("equipo",$propiedadesConexion)){
            $propiedadesConexion["equipo"]="127.0.0.1";
        }*/
        //  operador ternario completo
        //$propiedadesConexion["equipo"] = (array_key_exists("equipo",$propiedadesConexion)) ? $propiedadesConexion["equipo"] : "127.0.0.1";

        // operador ternario contraido (isset)
        // $propiedadesConexion["equipo"] = $propiedadesConexion["equipo"] ?? "127.0.0.1";


        $this->conexion = new mysqli(
            $propiedadesConexion["equipo"] ?? "127.0.0.1",
            $propiedadesConexion["usuario"] ?? "root",
            $propiedadesConexion["contrasena"] ?? "",
            $propiedadesConexion["basededatos"],
            $propiedadesConexion["puerto"] ?? 3306
        );
    }

    public function consulta($sql)
    {
        $this->consulta = $this->conexion->query($sql);
        return $this;
    }

    public function obtenerDatos()
    {
        return $this->consulta->fetch_all(MYSQLI_ASSOC);
    }
}
