<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    // crear un array
    $numeros = [];
    // $numeros=array();


    // creando un array asociativo inicializado
    // los indices son textos
    $numeroVocales = [
        'a' => 10,
        'e' => 1,
        'i' => 0,
        'o' => 5,
        'u' => 0
    ];

    // $numeroVocales = array(
    //     'a' => 10,
    //     'e' => 1,
    //     'i' => 0,
    //     'o' => 5,
    //     'u' => 0
    // );


    // leer el indice 'a'
    echo $numeroVocales['a']; //10
    ?>
</body>

</html>