<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    // crear un array 

    $numeros = [];
    // $numeros=array();


    // creando un array enumerado inicializado
    // los indices son numeros
    $vocales = ['a', 'e', 'i', 'o', 'u'];
    // $vocales=array('a','e','i','o','u');

    // leer la 'a'
    echo $vocales[0]; //a
    ?>
</body>

</html>