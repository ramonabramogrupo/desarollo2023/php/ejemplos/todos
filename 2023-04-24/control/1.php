<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    // Vamos a realizar un ejemplo con foreach

    $numeros = [1, 2, 45, 87, 24]; // array enumerado
    $letras = ['a', 's', 'j']; // array enumerado

    $notas = [
        'Jorge' => 10,
        'Cesar' => 6,
        'Eva' => 9
    ]; // array asociativo

    // Quiero mostrar el array numeros
    foreach ($numeros as $indice => $valor) {
        echo "{$indice}={$valor}<br>";
    }

    // mostrar el array numeros con un for
    // count($numeros)  // 5 (longitud del array)

    for ($indice = 0; $indice < count($numeros); $indice++) {
        $valor = $numeros[$indice];
        echo "{$indice}={$valor}<br>";
    }

    // recorrer el array letras con foreach
    foreach ($letras as $indice => $valor) {
        echo "{$indice}={$valor}<br>";
    }

    // recorrer el array letras con for
    for ($indice = 0; $indice < count($letras); $indice++) {
        $valor = $letras[$indice];
        echo "{$indice}={$valor}<br>";
    }


    // recorrer el array notas con foreach
    foreach ($notas as $indice => $valor) {
        echo "{$indice}={$valor}<br>";
    }


    ?>
</body>

</html>