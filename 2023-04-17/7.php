<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    $nombre = "ramon";
    $poblacion = null;


    // la funcion isset comprueba si la variable existe 
    // y no esta vacia
    var_dump(isset($nombre));  //true  (la variable existe)
    var_dump(isset($edad)); // false (la variable no existe)
    var_dump(isset($poblacion)); //false (la variable esta vacia)
    ?>
</body>

</html>