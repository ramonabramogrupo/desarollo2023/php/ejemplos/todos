<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        li {
            list-style-type: none;
            border: 1px solid black;
            margin: 1px;
            width: 200px;
            height: 50px;
            line-height: 50px;
            padding: 5px;
            transition: all 5s;
        }

        li:hover {
            background-color: #999;
        }
    </style>
</head>

<body>
    <?php
    $nombre = "Ramon";
    $edad = 50;
    $poblacion = "Santander";
    ?>
    <ul>
        <li>Nombre:<?= $nombre ?></li>
        <li>Edad:<?= $edad ?></li>
        <li>Poblacion:<?= $poblacion ?></li>
    </ul>
</body>

</html>