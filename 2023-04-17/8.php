<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    $nombre = "ramon";
    $poblacion = null; // vacio
    $apellidos = ""; // texto vacio


    // la funcion isset comprueba si la variable existe 
    // y no esta vacia
    var_dump(empty($nombre));  //false  (la variable no esta vacia)
    var_dump(empty($edad)); // true (la variable no existe)
    var_dump(empty($poblacion)); //true (la variable esta vacia)
    var_dump(isset($apellidos)); //true (la variable existe)
    var_dump(empty($apellidos)); //true (apellidos son texto vacio)
    ?>
</body>

</html>