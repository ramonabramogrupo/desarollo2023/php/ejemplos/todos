<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    // variables
    $edad = 0;

    // entradas
    $edad = 50;

    // procesamiento
    $edad = $edad + 1;  // 51
    $edad++;        // 52
    $edad += 1;       // 53

    // salidas

    echo $edad;             // 53
    echo "<br>";            // enter
    echo $edad . "<br>";    // 53 enter
    echo "$edad<br>";       // 53 enter
    echo '$edad<br>';       // $edad enter
    ?>

</body>

</html>