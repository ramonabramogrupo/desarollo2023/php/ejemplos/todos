<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    $nombre = "Ramon";
    $edad = 50;

    // mostrar cada variable dentro de un div
    // mostrar el nombre 
    // opcion 1
    echo "<div>$nombre</div>";
    //opcion 2
    echo "<div>" . $nombre . "</div>";
    // opcion 3
    echo "<div>";
    echo $nombre;
    echo "</div>";
    // opcion 4
    ?>
    <div>
        <?= $nombre ?>
    </div>
    <div>
        <?= $edad ?>
    </div>

</body>

</html>