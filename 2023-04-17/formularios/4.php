<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        li {
            list-style-type: none;
            border: 1px solid black;
            margin: 1px;
            width: 200px;
            height: 50px;
            line-height: 50px;
            padding: 5px;
            transition: all 5s;
        }

        li:hover {
            background-color: #999;
        }
    </style>
</head>

<body>
    <?php
    // mostrar la poblacion, provincia y calle que
    // escriba en el formulario
    $poblacion = "santander";
    $provincia = "Cantabria";
    $calle = "vargas";

    // Reglas
    // si no escribo nada en los 3 campos me debe utilizar
    // poblacion=santander
    // provincia=cantabria
    // calle=vargas
    // si alguno de ellos tiene algo escrito
    // los campos que no tengan nada dejarlos vacios

    if (isset($_GET["enviar"])) {
        if (
            !(empty($_GET["poblacion"]) &&
                empty($_GET["provincia"]) &&
                empty($_GET["calle"])
            )
        ) {
            $poblacion = $_GET["poblacion"];
            $calle = $_GET["calle"];
            $provincia = $_GET["provincia"];
        }
    }
    ?>
    <ul>
        <li>Poblacion:<?= $poblacion ?></li>
        <li>Provincia:<?= $provincia ?></li>
        <li>Calle: <?= $calle ?></li>
    </ul>
</body>

</html>