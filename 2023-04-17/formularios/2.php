<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        li {
            list-style-type: none;
            border: 1px solid black;
            margin: 1px;
            width: 200px;
            height: 50px;
            line-height: 50px;
            padding: 5px;
            transition: all 5s;
        }

        li:hover {
            background-color: #999;
        }
    </style>
</head>

<body>
    <?php
    // inicializando las variables
    $nombre = "";
    $edad = 0;
    $poblacion = "";

    //comprobar que el usuario a pulsado enviar
    //if($_GET){ // si vengo algun formulario
    if (isset($_GET["enviar"])) {  // si he pulsado el boton enviar del formulario
        // leer los datos del formulario
        $nombre = $_GET["nombre"];
        $edad = $_GET["edad"];
        $poblacion = $_GET["poblacion"];
    }


    //var_dump($nombre, $edad, $poblacion);
    ?>
    <ul>
        <li>Nombre:<?= $nombre ?></li>
        <li>Edad:<?= $edad ?></li>
        <li>Poblacion:<?= $poblacion ?></li>
    </ul>


</body>

</html>