<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        li {
            list-style-type: none;
            border: 1px solid black;
            margin: 1px;
            width: 200px;
            height: 50px;
            line-height: 50px;
            padding: 5px;
            transition: all 5s;
        }

        li:hover {
            background-color: #999;
        }
    </style>
</head>

<body>
    <?php
    // inicializando las variables
    $nombre = "no definido";
    $edad = "no definida";
    $poblacion = "no definida";

    //comprobar que el usuario a pulsado enviar
    if (isset($_GET["enviar"])) {
        // leer los datos del formulario
        if (!empty($_GET["nombre"])) { // si el nombre no esta vacio
            $nombre = $_GET["nombre"];
        }
        if (!empty($_GET["edad"])) { // si la edad no esta vacio
            $edad = $_GET["edad"];
        }
        if (!empty($_GET["poblacion"])) { // si poblacion no esta vacio
            $poblacion = $_GET["poblacion"];
        }
    }


    //var_dump($nombre, $edad, $poblacion);
    ?>
    <ul>
        <li>Nombre:<?= $nombre ?></li>
        <li>Edad:<?= $edad ?></li>
        <li>Poblacion:<?= $poblacion ?></li>
    </ul>


</body>

</html>