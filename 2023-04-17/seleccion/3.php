<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        li {
            list-style-type: none;
            border: 1px solid black;
            margin: 1px;
            width: 200px;
            height: 50px;
            line-height: 50px;
            padding: 5px;
            transition: all 5s;
        }

        li:hover {
            background-color: #999;
        }
    </style>
</head>

<body>
    <?php
    $dia = 3;
    $diaDeLaSemana = "";

    /*
        a que dia de la semana pertenece dia
        quiero utilizar switch
        */
    switch ($dia) {
        case 1:
            $diaDeLaSemana = "Lunes";
            break;
        case 2:
            $diaDeLaSemana = "Martes";
            break;
        case 3:
            $diaDeLaSemana = "Miercoles";
            break;
        case 4:
            $diaDeLaSemana = "Jueves";
            break;
        case 5:
            $diaDeLaSemana = "Viernes";
            break;
        case 6:
            $diaDeLaSemana = "Sabado";
            break;
        default:
            $diaDeLaSemana = "Domingo";
            break;
    }
    ?>
    <ul>
        <li>Dia:<?= $dia ?></li>
        <li>Dia de la semana:<?= $diaDeLaSemana ?></li>
    </ul>
</body>

</html>