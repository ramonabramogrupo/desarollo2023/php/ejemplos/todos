<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        li {
            list-style-type: none;
            border: 1px solid black;
            margin: 1px;
            width: 200px;
            height: 50px;
            line-height: 50px;
            padding: 5px;
            transition: all 5s;
        }

        li:hover {
            background-color: #999;
        }
    </style>
</head>

<body>
    <?php
    $diaDeLaSemana = "jueves";
    $dia = 0;

    /*
        a que dia en numero pertenece el dia de la semana
        quiero utilizar switch
        */
    switch ($diaDeLaSemana) {
        case "lunes":
            $dia = 1;
            break;
        case "martes":
            $dia = 2;
            break;
        case "miercoles":
            $dia = 3;
            break;
        case "jueves":
            $dia = 4;
            break;
        case "viernes":
            $dia = 5;
            break;
        case "sabado":
            $dia = 6;
            break;
        case "domingo":
            $dia = 7;
            break;
        default:
            $dia = 'incorrecto';
            break;
    }
    ?>
    <ul>
        <li>Dia:<?= $dia ?></li>
        <li>Dia de la semana:<?= $diaDeLaSemana ?></li>
    </ul>
</body>

</html>