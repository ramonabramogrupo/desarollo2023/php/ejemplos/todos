<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        li {
            list-style-type: none;
            border: 1px solid black;
            margin: 1px;
            width: 200px;
            height: 50px;
            line-height: 50px;
            padding: 5px;
            transition: all 5s;
        }

        li:hover {
            background-color: #999;
        }
    </style>
</head>

<body>
    <?php
    $salida = "";
    $nota = 8;

    /*
    <5 : suspenso
    entre 5 y 7 : aprobado
    entre 7 y 9 : notables
    >=9 : sobresaliente
    */

    if ($nota < 5) {
        $salida = "suspenso";
    } elseif ($nota < 7) {
        $salida = "aprobado";
    } elseif ($nota < 9) {
        $salida = "notable";
    } else {
        $salida = "sobresaliente";
    }
    ?>
    <ul>
        <li>Nota:<?= $nota ?></li>
        <li>Estado:<?= $salida ?></li>
    </ul>
</body>

</html>