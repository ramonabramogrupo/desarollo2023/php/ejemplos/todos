<?php
// creamos una conexion
// creamos un objeto de tipo conexion
$conexion = new mysqli(
    "localhost", // servidor de base de datos
    "root",      // usuario
    "",         // contraseña
    "desarrollo", // base de datos
    3306        // puerto
);

// realizar nuestra primera consulta
// quiero sacar los alumnos

$resultado = $conexion->query("select * from alumnos");

// leo todos los registros de la tabla
// como array ENUMERADO de arrays ASOCIATIVOS
$filas = $resultado->fetch_all(MYSQLI_ASSOC);
var_dump($filas);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <table border="1">
        <thead style="background-color:#ccc">
            <tr>
                <td>Codigo</td>
                <td>Nombre</td>
                <td>Correo</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <?= $filas[0]["codigo"] ?>
                </td>
                <td>
                    <?= $filas[0]["nombre"] ?>
                </td>
                <td>
                    <?= $filas[0]["correo"] ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?= $filas[1]["codigo"] ?>
                </td>
                <td>
                    <?= $filas[1]["nombre"] ?>
                </td>
                <td>
                    <?= $filas[1]["correo"] ?>
                </td>
            </tr>
        </tbody>
    </table>

</body>

</html>