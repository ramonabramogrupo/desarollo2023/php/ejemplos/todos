<?php
// creamos una conexion
// creamos un objeto de tipo conexion
$conexion = new mysqli(
    "localhost", // servidor de base de datos
    "root",      // usuario
    "",         // contraseña
    "desarrollo", // base de datos
    3306        // puerto
);

// realizar nuestra primera consulta
// quiero sacar los alumnos

$resultado = $conexion->query("select * from alumnos");

// leo el primer registro de la tabla
// como array asociativo
$fila = $resultado->fetch_assoc();
var_dump($fila);
/* $fila =[
    "codigo" => 1,
    "nombre" => "rosa",
    "correo" => "aaaa@aa.es"
];*/

// leo el segundo registro de la tabla
$fila = $resultado->fetch_assoc();

var_dump($fila);


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <table border="1">
        <thead style="background-color:#ccc">
            <tr>
                <td>Codigo</td>
                <td>Nombre</td>
                <td>Correo</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <?= $fila["codigo"] ?>
                </td>
                <td>
                    <?= $fila["nombre"] ?>
                </td>
                <td>
                    <?= $fila["correo"] ?>
                </td>
            </tr>
        </tbody>
    </table>

</body>

</html>