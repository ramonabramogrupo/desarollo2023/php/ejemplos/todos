<?php
// creamos una conexion
// creamos un objeto de tipo conexion
$conexion = new mysqli(
    "localhost", // servidor de base de datos
    "root",      // usuario
    "",         // contraseña
    "desarrollo", // base de datos
    3306        // puerto
);

// realizar nuestra primera consulta
// quiero sacar los alumnos

$resultado = $conexion->query("select * from alumnos");

// leo el primer registro de la tabla
// como array mixto (asociativo y enumerado)
$fila = $resultado->fetch_array();
var_dump($fila);
/* $fila =[
    0=>1,
    "codigo" => 1,
    1=>"rosa,
    "nombre" => "rosa",
    2=>"aaaa@aa.es",
    "correo" => "aaaa@aa.es"
];*/



?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <table border="1">
        <thead style="background-color:#ccc">
            <tr>
                <td>Codigo</td>
                <td>Nombre</td>
                <td>Correo</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <?= $fila["codigo"] ?>
                </td>
                <td>
                    <?= $fila["nombre"] ?>
                </td>
                <td>
                    <?= $fila["correo"] ?>
                </td>
            </tr>
        </tbody>
    </table>

</body>

</html>