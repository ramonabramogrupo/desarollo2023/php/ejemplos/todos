<?php

namespace clases\elementos;

class Coche
{
    public $marca;
    public $modelo;
    public $cilindrada;
    private $bastidor;

    /**
     * __construct
     *
     * @param  string $marca Marca del coche
     * @param  string $modelo Modelo del coche
     * @return void
     */
    public function __construct($marca, $modelo)
    {
        $this->marca = $marca;
        $this->modelo = $modelo;
    }

    /*
    metodo magico que se llama cuando intente
    imprimir el objeto 
    */
    public function __toString()
    {
        return "{$this->marca}-{$this->modelo}";
    }

    /**
     * Get the value of bastidor
     */
    public function getBastidor()
    {
        return $this->bastidor;
    }

    /**
     * Set the value of bastidor
     *
     * @return  self
     */
    public function setBastidor($bastidor)
    {
        // regla de negocio (restriccion, rules)
        if (!empty($bastidor)) {
            $this->bastidor = $bastidor;
        }
        return $this;
    }

    public function dibujarTabla()
    {
        ob_start();
?>
        <table border="1">
            <thead>
                <tr>
                    <td>
                        Marca
                    </td>
                    <td>
                        Modelo
                    </td>
                    <td>
                        bastidor
                    </td>
                    <td>
                        Cilindrada
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?= $this->marca ?></td>
                    <td><?= $this->modelo ?></td>
                    <td><?= $this->bastidor ?></td>
                    <td><?= $this->cilindrada ?></td>
                </tr>
            </tbody>
        </table>
<?php
        return ob_get_clean();
    }
}
