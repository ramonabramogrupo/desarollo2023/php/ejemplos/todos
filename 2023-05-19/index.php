<?php

use clases\elementos\Coche;

spl_autoload_register(function ($nombreClase) {
    require_once "$nombreClase.php";
});

$coche1 = new Coche(
    "Ford",
    "Mondeo"
);

var_dump($coche1);

echo $coche1->dibujarTabla();

$coche1->setBastidor("123456789");
echo $coche1->getBastidor();
echo $coche1->dibujarTabla();

$coche1->cilindrada = 3000;

echo $coche1->dibujarTabla();
