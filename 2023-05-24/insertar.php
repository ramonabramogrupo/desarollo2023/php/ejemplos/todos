<?php

use clases\elementos\Moto;
use clases\librerias\Conexion;

spl_autoload_register(function ($nombreClase) {
    require_once "$nombreClase.php";
});
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/main.css">
    <title>Document</title>
</head>

<body>

    <?php
    require_once "_menu.php";
    if (isset($_GET["enviar"])) {
        // recupero los datos del formulario y los almaceno en la tabla
        $conexion = new Conexion([
            "basededatos" => "concesionarioMotos"
        ]);

        extract($_GET, EXTR_PREFIX_SAME, "formulario");

        $modelo = new Moto([
            "id" => $id,
            "matricula" => $matricula,
            "marca" => $marca,
            "modelo" => $modelo,
            "precio" => $precio
        ]);

        $modelo->insertar($conexion);
        header("Location: index.php");
        // echo $modelo->dibujarTabla();
    } else {
        $modelo = new Moto();
        // carga el formulario 
        require_once "_form.php";
    }
    ?>

</body>

</html>