<?php
spl_autoload_register(function ($nombreClase) {
    require_once "$nombreClase.php";
});

use clases\librerias\Conexion;
use clases\elementos\Moto;

$conexion = new Conexion([
    "basededatos" => "concesionarioMotos",
]);

// tengo los datos del coche en un array
$datos=$conexion->consulta(
    "select * from moto where id='{$_GET["id"]}'"
)->obtenerDatos()[0];

// tengo los datos de la moto en un modelo (objeto)
$modelo = new Moto([
            "id" => $datos["id"],
            "matricula" => $datos["matricula"],
            "marca" => $datos["marca"],
            "modelo" => $datos["modelo"],
            "precio" => $datos["precio"]
        ]);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
    <?php
    require_once "_menu.php";
    //echo $datos["bastidor"];
    echo $modelo->dibujarTabla();
    ?>
</body>
</html>