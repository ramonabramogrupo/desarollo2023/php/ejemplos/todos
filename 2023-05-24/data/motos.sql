﻿DROP DATABASE IF EXISTS concesionarioMotos;
CREATE DATABASE concesionarioMotos;
USE concesionarioMotos;

CREATE TABLE moto(
id int,
matricula varchar(10),
marca varchar(100),
modelo varchar(100),
precio float,
PRIMARY KEY(id),
UNIQUE KEY(matricula)
);

INSERT INTO moto (id, matricula, marca, modelo, precio)
  VALUES (1, 'dgh1234', 'yamaha', 'tmax', 10000);

