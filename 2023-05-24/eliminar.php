<?php
session_start(); // para que funcionen las sesiones

use clases\elementos\Moto;
use clases\librerias\Conexion;

spl_autoload_register(function ($nombreClase) {
    require_once "$nombreClase.php";
});
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
    <?php
    require_once "_menu.php";
    // entro cuando he pulsado eliminar
    if(isset($_GET["id"])){
        $conexion = new Conexion([
            "basededatos" => "concesionarioMotos"
        ]);

         // recuperando los datos del coche a modificar de la bbddd
        $datos = $conexion
            ->consulta("
        select * from moto where id='{$_GET["id"]}'
        ")
            ->obtenerDatos()[0];

         $modelo = new Moto([
            "id" => $datos["id"],
            "matricula" => $datos["matricula"],
            "marca" => $datos["marca"],
            "modelo" => $datos["modelo"],
            "precio" => $datos["precio"]
        ]);
        
        $_SESSION["id"]=$_GET["id"];

        require_once "_ver.php";

    // compruebo si he pulsado el boton de eliminar en la confirmacion
    }elseif(isset($_GET["eliminar"])){
        $conexion = new Conexion([
            "basededatos" => "concesionarioMotos"
        ]);

         // recuperando los datos del coche a modificar de la bbddd
        $datos = $conexion
            ->consulta("
        select * from moto where id='{$_SESSION["id"]}'
        ")
            ->obtenerDatos()[0];

       $modelo = new Moto([
            "id" => $datos["id"],
            "matricula" => $datos["matricula"],
            "marca" => $datos["marca"],
            "modelo" => $datos["modelo"],
            "precio" => $datos["precio"]
        ]);

        $modelo->eliminar($conexion);
        echo "el registro se ha eliminado correctamente";
    
    }else{
        echo "No tengo los datos del coche a eliminar";
    }
    ?>
    <div>
    <a href="index.php">Volver a index</a>
    </div>
</body>
</html>