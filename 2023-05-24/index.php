<?php

use clases\librerias\Conexion;
use clases\librerias\Utilidades;

spl_autoload_register(function ($nombreClase) {
    require_once "$nombreClase.php";
});
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/main.css">
    <title>Document</title>
</head>

<body>
    <?php
    require_once "_menu.php";
    $conexion = new Conexion([
        "basededatos" => "concesionarioMotos"
    ]);

    $datos = $conexion
        ->consulta("select * from moto")
        ->obtenerDatos();

    echo Utilidades::gridView($datos,[],"id");

    ?>
</body>

</html>