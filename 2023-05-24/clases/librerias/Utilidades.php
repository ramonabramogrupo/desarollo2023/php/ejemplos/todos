<?php

namespace clases\librerias;

class Utilidades
{
    static function gridView($registros, $campos = [], $clave = "")
    {
        ob_start();
        // comprobar si no hemos pasado nada en campos
        // leo los campos del primer registro
        if (empty($campos)) {
            // array_keys
            // esta funcion te devuelve todos los indices de un array 
            // $a=[
            //  "nombre" =>"ramon",
            // "poblacion" => "santander"  
            // ];
            // $b=array_keys($a);
            // $b tendria esto ["nombre","poblacion"];
            $campos = array_keys($registros[0]); // lee todos los campos del primer registro
        }
?>
        <table class="gridView">
            <thead>
                <tr>
                    <?php
                    foreach ($campos as $titulo) {
                        echo "<td>{$titulo}</td>";
                    }
                    if (!empty($clave)) {
                        echo "<td>Acciones</td>";
                    }

                    ?>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($registros as $registro) {
                ?>
                    <tr>
                        <?php
                        foreach ($campos as $titulo) {
                            echo "<td>{$registro[$titulo]}</td>";
                        }
                        if (!empty($clave)) {
                        ?>
                            <td>
                                <a href="actualizar.php?<?= "{$clave}={$registro[$clave]}" ?>"> Actualizar</a>
                                <a href="eliminar.php?<?= "{$clave}={$registro[$clave]}" ?>"> Eliminar</a>
                                <a href="Ver.php?<?= "{$clave}={$registro[$clave]}" ?>"> Ver</a>
                            </td>
                        <?php

                        }

                        ?>
                    </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
<?php
        return ob_get_clean();
    }
    static public function obtenerNombrePagina()
    {
        // necesito conocer la pagina php que carga la vista

        // constantes de servidor
        // __FILE__ nombre del archivo php con la ruta

        // var_dump(__FILE__); // ruta absoluta y nombre
        // 'C:\laragon\www\desarrollo2023\php\ejemplos\2023-05-12\_menu.php'

        // variables del servidor
        // $_SERVER es un array con datos del servidor
        // var_dump($_SERVER['PHP_SELF']); // ruta relativa y nombre
        // '/php/ejemplos/2023-05-12/index.php'


        // sacar el nombre del archivo que se esta ejecutando
        $a = $_SERVER['PHP_SELF']; // ruta y el nombre

        // primera opcion
        // strrchr 
        // busca la ultima aparicion de un texto dentro de un string 
        // y me devuelve el texto que hay desde esa ultima aparicion
        $b = strrchr($a, '/');

        // substr(texto,inicio,longitud)
        $pagina = substr($b, 1);

        // segunda opcion
        // explode
        // crea un array de un string utilizando un caracter 
        $b = explode("/", $a);

        // array_pop
        // extrae el ultimo elemento de un array
        $pagina = array_pop($b);


        // resumen
        // $a= $_SERVER['PHP_SELF'];
        // $b=explode("/",$a);
        // $pagina=array_pop($b);

        return $pagina;
    }


    static public function dibujarMenu($menu)
    {
        $paginaActiva = self::obtenerNombrePagina();
        echo "<ul>";
        // dibujar el menu utilizando foreach
        foreach ($menu as $etiqueta => $href) {
            if ($href == $paginaActiva) {
                echo "<li class=\"activo\"><a href=\"{$href}\">{$etiqueta}</a></li>";
            } else {
                echo "<li><a href=\"{$href}\">{$etiqueta}</a></li>";
            }
        }
        echo "</ul>";
        }
    }
