<?php

namespace clases\elementos;

class Moto
{
    public $id;
    public $matricula;
    public $marca;
    public $modelo;
    public $precio;
    private $idOld;

    /**
     * __construct
     *
     * @param  array $datos debes pasar un array asociativo con las propiedades a inicializar
     * son id,matricula,marca, modelo, precio
     * @return void
     */
    public function __construct($datos = [])
    {
        $this->marca = $datos["marca"] ?? "";
        $this->modelo = $datos["modelo"] ?? "";
        $this->precio = $datos["precio"] ?? 0;
        $this->id = $datos["id"] ?? 1;
        $this->matricula = $datos["matricula"] ?? "";
        $this->idOld = $datos["idOld"] ?? "";
    }

    /*
    metodo magico que se llama cuando intente
    imprimir el objeto 
    */
    public function __toString()
    {
        return "{$this->marca}-{$this->modelo}";
    }


    public function dibujarTabla()
    {
        ob_start();
?>
        <table border="1">
            <thead>
                <tr>
                    <td>
                        Marca
                    </td>
                    <td>
                        Modelo
                    </td>
                    <td>
                        id
                    </td>
                    <td>
                        precio
                    </td>
                    <td>
                        matricula
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?= $this->marca ?></td>
                    <td><?= $this->modelo ?></td>
                    <td><?= $this->id ?></td>
                    <td><?= $this->precio ?></td>
                    <td><?= $this->matricula ?></td>
                </tr>
            </tbody>
        </table>
<?php
        return ob_get_clean();
    }

    public function insertar($conexion)
    {
        $conexion->consulta("
        insert into moto values($this->id,'$this->matricula','$this->marca','$this->modelo',$this->precio)
        ");
    }

    public function actualizar($conexion)
    {
        $conexion->consulta("
        update moto set 
            id=$this->id,
            marca='$this->marca', 
            modelo='$this->modelo', 
            precio=$this->precio,
            matricula='$this->matricula'
            where id=$this->idOld
        ");
    }
    public function eliminar($conexion){

        $conexion->consulta("
            DELETE FROM moto WHERE id='{$this->id}'
        ");

    }

}
