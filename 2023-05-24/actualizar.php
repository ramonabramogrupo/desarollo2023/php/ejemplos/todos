<?php
session_start(); // para que funcionen las sesiones

use clases\elementos\Moto;
use clases\librerias\Conexion;

spl_autoload_register(function ($nombreClase) {
    require_once "$nombreClase.php";
});
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/main.css">
    <title>Document</title>
</head>

<body>
    <?php
    require_once "_menu.php";
    $conexion = new Conexion([
        "basededatos" => "concesionarioMotos"
    ]);

    // he pulsado el boton de actualizar
    if (isset($_GET["enviar"])) {

        $modelo = new Moto([
            "id" => $_GET["id"],
            "matricula" => $_GET["matricula"],
            "marca" => $_GET["marca"],
            "modelo" => $_GET["modelo"],
            "precio" => $_GET["precio"],
            "idOld" => $_SESSION["id"],
          
        ]);

        $modelo->actualizar($conexion);

        header("Location: index.php");
    } else {

        // leo el id de la moto que quiero modificar los datos
        // y lo almaceno en una variables session
        $_SESSION["id"] = $_GET["id"];

        // recuperando los datos del coche a modificar de la bbddd
        $datos = $conexion
            ->consulta("
        select * from moto where id='{$_SESSION["id"]}'
        ")
            ->obtenerDatos();

        $modelo = new Moto([
            "id" => $datos[0]["id"],
            "matricula" => $datos[0]["matricula"],
            "marca" => $datos[0]["marca"],
            "modelo" => $datos[0]["modelo"],
            "precio" => $datos[0]["precio"]
        ]);

        require_once("_form.php");
    }
    ?>
</body>

</html>